Cześć,
to moje zadanko rekrutacyjne,
generalne z pozytywnych rzeczy jest to że zrobiłem,
a z negatywnych, że lekko po czasie,
za długo bawiłem się w niepotrzebne w sumie biblioteki i na tym straciłem sporo czasu.
Funkcjonalnie wszystko śmiga, widoki są do poprawek wizualnych (np. odstępy) ale to już
kosmetyczna zabawa.
Użyte pomoce to:
retrofit
hilt
room
cardview
picasso
navigation graph
Pzdr Patryk

Treść zadania

Utwórz aplikację prezentującą listę użytkowników oraz po kliknięciu w wybranego jego szczegóły.
1. Listą elementów.
○ Dane dla aplikacji mają pochodzić z dwóch różnych API (podane na końcu dokumentu)
○ Utwórz bazę danych z wykorzystaniem Room’a i zapisz wyniki z obu API do jednej tabeli.
○ Przy starcie aplikacja powinna zapisywać dane z obu API do bazy
○ Elementy należy wyciągnąć tylko z bazy i zaprezentować przy pomocy RecyclerView.
○ Listę należy obsadzić we Fragmencie
○ Element na liście powinien posiadać informacje:
■ nazwę użytkownika
■ zdjęcie / avatar
■ źródłowe api
○ Po kliknięciu elementu ma otworzyć się nowy ekran ze szczegółami (pkt 2)

2. Activity ze szczegółami ma być prostą prezentacją wcześniej wybranego elementu z listy
○ ma posiadać informacje:
■ nazwę użytkownika
■ zdjęcie / avatar
■ źródłowe api
Wymagania ogólne:
● użycie wzorca projektowego MVP lub MVVM (użycie ViewModel z Architecture
Components oraz DataBinding będzie dodatkowym atutem) Endpoint’y:

● APIs
https://api.dailymotion.com/users
https://api.github.com/users