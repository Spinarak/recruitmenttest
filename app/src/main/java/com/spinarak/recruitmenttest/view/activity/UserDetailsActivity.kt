package com.spinarak.recruitmenttest.view.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import com.spinarak.recruitmenttest.databinding.ActivityUserDetailsBinding
import com.spinarak.recruitmenttest.viewmodel.UserDetailsActivityViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserDetailsBinding
    private val viewModel: UserDetailsActivityViewModel by viewModels()
    private val args: UserDetailsActivityArgs by navArgs<UserDetailsActivityArgs>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUserDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getUserDetails()
    }

    private fun getUserDetails(){
        viewModel.userDetails(args.userId)
        viewModel.user.observe(this) {
            val user = viewModel.user.value
            user?.let {
                binding.apiName.text = user.apiName
                binding.username.text = user.userName
                it.avatarUrl?.let {
                    Picasso.get().load(it).into(binding.userThumbnail)
                }
            }
        }
    }
}