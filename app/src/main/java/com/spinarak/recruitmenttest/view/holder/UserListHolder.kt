package com.spinarak.recruitmenttest.view.holder

import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.spinarak.recruitmenttest.databinding.UserItemBinding
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.view.fragment.UserListFragmentDirections
import com.squareup.picasso.Picasso


class UserListHolder(
        private val binding: UserItemBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(user: User) {
        binding.username.text = user.userName
        binding.apiName.text = user.apiName
        user.avatarUrl?.let {
            Picasso.get().load(it).into(binding.userThumbnail)
        } ?: run {
            binding.userThumbnail.setImageDrawable(null)
        }
        binding.detailsButton.setOnClickListener {
            itemView.findNavController().navigate(UserListFragmentDirections.actionUserDetails(user.id))
        }
    }
}