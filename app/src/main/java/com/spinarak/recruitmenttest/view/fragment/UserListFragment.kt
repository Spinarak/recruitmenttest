package com.spinarak.recruitmenttest.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.spinarak.recruitmenttest.databinding.FragmentUsersListBinding
import com.spinarak.recruitmenttest.view.adapter.UserListAdapter
import com.spinarak.recruitmenttest.viewmodel.UserListFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import androidx.lifecycle.viewModelScope


@AndroidEntryPoint
class UserListFragment : Fragment() {

    private lateinit var binding: FragmentUsersListBinding
    private val viewModel: UserListFragmentViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentUsersListBinding.inflate(layoutInflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewModelScope.launch {
            viewModel.getListOfUsers()
        }
        initUsersRecyclerView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    private fun initUsersRecyclerView() = binding.socialUsers.apply {
        layoutManager = LinearLayoutManager(activity)
        setHasFixedSize(true)
        adapter = UserListAdapter(emptyList())
        viewModel.usersList.observe(viewLifecycleOwner) {
            (adapter as UserListAdapter).submitList(it)
        }
    }
}