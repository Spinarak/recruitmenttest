package com.spinarak.recruitmenttest.view.adapter


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.spinarak.recruitmenttest.databinding.UserItemBinding
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.view.holder.UserListHolder


class UserListAdapter(var users: List<User>) : RecyclerView.Adapter<UserListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListHolder {
        return UserListHolder(UserItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
        ))
    }

    override fun onBindViewHolder(holder: UserListHolder, position: Int) {
        val status: User = users[position]
        holder.bind(status)

    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun submitList(newData: List<User>) {
        users = newData
        notifyDataSetChanged()
    }

}