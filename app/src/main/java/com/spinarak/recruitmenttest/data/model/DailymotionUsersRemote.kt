package com.spinarak.recruitmenttest.data.model;

data class DailymotionUsersRemote(
    val id: String,
    val screenname: String
)
