package com.spinarak.recruitmenttest.data.api

import com.spinarak.recruitmenttest.data.dto.DailymotionUsersDTO
import retrofit2.Response
import retrofit2.http.GET

interface DailymotionService {
    @GET("users")
    suspend fun getAllDailymotionUsers(): Response<DailymotionUsersDTO>
}