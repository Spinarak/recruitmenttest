package com.spinarak.recruitmenttest.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.spinarak.recruitmenttest.data.database.users.UsersDAO
import com.spinarak.recruitmenttest.data.database.users.UsersEntity

@Database(
        entities = [
            UsersEntity::class,
        ],
        version = 1,
        exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun usersDAO(): UsersDAO
}