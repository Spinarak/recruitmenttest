package com.spinarak.recruitmenttest.data.repository

import com.spinarak.recruitmenttest.common.util.map
import com.spinarak.recruitmenttest.data.api.DailymotionService
import com.spinarak.recruitmenttest.data.api.GithubService
import com.spinarak.recruitmenttest.data.mapper.DailymotionDTOMapper
import com.spinarak.recruitmenttest.data.mapper.GithubDTOMapper
import com.spinarak.recruitmenttest.domain.model.Users
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SocialRepositoryRemote @Inject constructor(
        private val githubService: GithubService,
        private val dailymotionService: DailymotionService
) : SocialUsersRepository.Remote {
    override fun getGithubUsers() : Flow<Users?> {
        return flow{
            emit(
                  githubService.getAllGithubUsers().map(GithubDTOMapper())
            )
        }
    }
    override fun getDailymotionUsers() : Flow<Users?> {
        return flow{
            emit(
                dailymotionService.getAllDailymotionUsers().map(DailymotionDTOMapper())
            )
        }
    }
}