package com.spinarak.recruitmenttest.data.mapper

import com.spinarak.recruitmenttest.common.Consts.DAILYMOTION_API_NAME
import com.spinarak.recruitmenttest.common.util.Mapper
import com.spinarak.recruitmenttest.data.dto.DailymotionUsersDTO
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.model.Users
import retrofit2.Response

class DailymotionDTOMapper : Mapper<Response<DailymotionUsersDTO>, Users?> {
    override fun Response<DailymotionUsersDTO>.map(): Users? {
        return this.body()?.let {
            Users(it.list.map { user ->
                User(
                    user.id,
                    null,
                    user.screenname,
                    DAILYMOTION_API_NAME
                )
            })
        }
    }
}