package com.spinarak.recruitmenttest.data.api

import com.spinarak.recruitmenttest.data.dto.GithubUsersDTO
import com.spinarak.recruitmenttest.data.model.GithubUsersRemote
import retrofit2.Response
import retrofit2.http.GET

interface GithubService {
    @GET("users")
    suspend fun getAllGithubUsers(): Response<List<GithubUsersDTO>>
}