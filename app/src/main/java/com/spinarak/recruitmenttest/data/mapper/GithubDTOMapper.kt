package com.spinarak.recruitmenttest.data.mapper

import com.spinarak.recruitmenttest.common.Consts.GITHUB_API_NAME
import com.spinarak.recruitmenttest.common.util.Mapper
import com.spinarak.recruitmenttest.data.dto.GithubUsersDTO
import com.spinarak.recruitmenttest.data.model.GithubUsersRemote
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.model.Users
import retrofit2.Response

class GithubDTOMapper : Mapper<Response<List<GithubUsersDTO>>, Users?> {
    override fun Response<List<GithubUsersDTO>>.map(): Users? {
        return this.body()?.let{
            Users(
                it.map { user ->
                    User(
                        user.id.toString(),
                        user.avatarUrl,
                        user.login,
                        GITHUB_API_NAME
                    )
                }
            )
        }
    }
}