package com.spinarak.recruitmenttest.data.repository

import com.spinarak.recruitmenttest.common.util.map
import com.spinarak.recruitmenttest.data.database.users.UsersDAO
import com.spinarak.recruitmenttest.data.mapper.SocialUsersToUsersEntityMapper
import com.spinarak.recruitmenttest.data.mapper.UsersEntityToSocialUsersMapper
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SocialRepositoryLocal @Inject constructor(
        private val usersDAO: UsersDAO
) : SocialUsersRepository.Local {
    override suspend fun saveSocialUsers(users: List<User>) {
        usersDAO.insertAll(users.map(SocialUsersToUsersEntityMapper()))
    }

    override suspend fun getSocialUsers(): Flow<List<User>> = flow {
        emit(usersDAO.getAll().map(UsersEntityToSocialUsersMapper()))
    }

    override suspend fun getSocialUsersById(id: String): Flow<User> = flow {
        emit(usersDAO.getUsersById(id).map(UsersEntityToSocialUsersMapper()))
    }
}