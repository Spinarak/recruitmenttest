package com.spinarak.recruitmenttest.data.database.users

import androidx.room.*
import com.spinarak.recruitmenttest.domain.model.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UsersDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: UsersEntity) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(users: List<UsersEntity>)

    @Update
    suspend fun update(user: UsersEntity)

    @Transaction
    @Query("SELECT * FROM users_table")
    suspend fun getAll(): MutableList<UsersEntity>

    @Query("DELETE FROM users_table")
    suspend fun deleteAll()

    @Query("SELECT * FROM users_table WHERE id LIKE :id ")
    suspend fun getUsersById(id: String): UsersEntity

}