package com.spinarak.recruitmenttest.data.modules

import com.spinarak.recruitmenttest.data.api.DailymotionService
import com.spinarak.recruitmenttest.data.api.GithubService
import com.spinarak.recruitmenttest.data.repository.SocialRepositoryLocal
import com.spinarak.recruitmenttest.data.repository.SocialRepositoryRemote
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindSocialUsersRepositoryRemote(
            socialRepository: SocialRepositoryRemote
    ): SocialUsersRepository.Remote

    @Binds
    abstract fun bindSocialUsersRepositoryLocal(
            socialRepository: SocialRepositoryLocal
    ): SocialUsersRepository.Local
}