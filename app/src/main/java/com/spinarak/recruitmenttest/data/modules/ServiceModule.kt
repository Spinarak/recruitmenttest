package com.spinarak.recruitmenttest.data.modules


import com.spinarak.recruitmenttest.common.Consts.CONTENT_TYPE
import com.spinarak.recruitmenttest.common.Consts.DAILYMOTION_BASE_URL
import com.spinarak.recruitmenttest.common.Consts.GITHUB_BASE_URL
import com.spinarak.recruitmenttest.data.api.DailymotionService
import com.spinarak.recruitmenttest.data.api.GithubService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Singleton
    @Provides
    fun provideGithubService(): GithubService {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
        val contentType = CONTENT_TYPE.toMediaType()
        return Retrofit.Builder()
                .baseUrl(GITHUB_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(GithubService::class.java)
    }

    @Singleton
    @Provides
    fun provideDailymotionService(): DailymotionService {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
        val contentType = CONTENT_TYPE.toMediaType()
        return Retrofit.Builder()
                .baseUrl(DAILYMOTION_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(DailymotionService::class.java)
    }
}