package com.spinarak.recruitmenttest.data.dto

import com.google.gson.annotations.SerializedName
import com.spinarak.recruitmenttest.data.model.DailymotionUsersRemote

data class DailymotionUsersDTO(
        @SerializedName("list")
        val list: List<DailymotionUsersRemote>,
        @SerializedName("page")
        val page: Int? = null,
        @SerializedName("limit")
        val limit: Int? = null,
        @SerializedName("explicit")
        val explicit: Boolean? = null,
        @SerializedName("total")
        val total: Long? = null,
        @SerializedName("has_more")
        val hasMore: Boolean? = null
)