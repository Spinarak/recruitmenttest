package com.spinarak.recruitmenttest.data.mapper

import com.spinarak.recruitmenttest.common.util.Mapper
import com.spinarak.recruitmenttest.data.database.users.UsersEntity
import com.spinarak.recruitmenttest.domain.model.User

class UsersEntityToSocialUsersMapper : Mapper<UsersEntity, User> {
    override fun UsersEntity.map(): User = User(this.id, this.avatarUrl, this.userName, this.apiName)
}