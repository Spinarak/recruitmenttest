package com.spinarak.recruitmenttest.data.modules

import android.content.Context
import androidx.room.Room
import com.spinarak.recruitmenttest.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    fun provideCartDatabase(@ApplicationContext context: Context) =
            Room.databaseBuilder(
                    context, AppDatabase::class.java, "app_database"
            ).build()

    @Provides
    fun provideUsersDao(db: AppDatabase) = db.usersDAO()
}