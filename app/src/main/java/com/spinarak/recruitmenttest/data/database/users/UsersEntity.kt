package com.spinarak.recruitmenttest.data.database.users

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users_table")
data class UsersEntity (

        @PrimaryKey(autoGenerate = false)
        val id: String,

        @ColumnInfo(name = "avatar_url")
        val avatarUrl: String?,

        @ColumnInfo(name = "user_name")
        val userName: String,

        @ColumnInfo(name = "api_name")
        val apiName: String

)