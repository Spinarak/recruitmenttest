package com.spinarak.recruitmenttest.data.mapper

import com.spinarak.recruitmenttest.common.util.Mapper
import com.spinarak.recruitmenttest.data.database.users.UsersEntity
import com.spinarak.recruitmenttest.domain.model.User

class SocialUsersToUsersEntityMapper : Mapper<User, UsersEntity> {
    override fun User.map(): UsersEntity = UsersEntity(this.id, this.avatarUrl, this.userName, this.apiName)
}