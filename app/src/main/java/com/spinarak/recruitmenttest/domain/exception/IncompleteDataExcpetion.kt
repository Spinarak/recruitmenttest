package com.spinarak.recruitmenttest.domain.exception

class IncompleteDataException(message: String) : Exception(message)