package com.spinarak.recruitmenttest.domain.repository

import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.model.Users
import kotlinx.coroutines.flow.Flow

interface SocialUsersRepository {
    interface Remote {
        fun getGithubUsers(): Flow<Users?>

        fun getDailymotionUsers(): Flow<Users?>
    }

    interface Local {
        suspend fun saveSocialUsers(users: List<User>)

        suspend fun getSocialUsers(): Flow<List<User>>

        suspend fun getSocialUsersById(id: String) : Flow<User>
    }
}