package com.spinarak.recruitmenttest.domain.usecase

import com.spinarak.recruitmenttest.common.usecase.FlowSuspendUseCase
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import javax.inject.Inject

class GetSocialUsersLocalUseCase @Inject constructor(
        private val socialUsersRepository: SocialUsersRepository.Local
) : FlowSuspendUseCase<List<User>> {
    override suspend fun execute() = socialUsersRepository.getSocialUsers()
}