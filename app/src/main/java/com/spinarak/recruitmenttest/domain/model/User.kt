package com.spinarak.recruitmenttest.domain.model

data class User(
        val id: String,
        val avatarUrl: String?,
        val userName: String,
        val apiName: String
)
