package com.spinarak.recruitmenttest.domain.usecase

import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import javax.inject.Inject

class SetSocialUsersLocalUseCase @Inject constructor(
        private val socialUsersRepository: SocialUsersRepository.Local
) {
    suspend fun execute(usersList: List<User>) = socialUsersRepository.saveSocialUsers(usersList)
}