package com.spinarak.recruitmenttest.domain.usecase

import com.spinarak.recruitmenttest.common.usecase.FlowParameterUseCase
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import javax.inject.Inject

class GetSocialUserUseCase @Inject constructor(
        private val socialUsersRepository: SocialUsersRepository.Local
) : FlowParameterUseCase<String, User> {
    override suspend fun execute(params: String) = socialUsersRepository.getSocialUsersById(params)
}