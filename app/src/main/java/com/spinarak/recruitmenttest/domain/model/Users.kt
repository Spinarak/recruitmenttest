package com.spinarak.recruitmenttest.domain.model

data class Users(
        val usersList: List<User>
)