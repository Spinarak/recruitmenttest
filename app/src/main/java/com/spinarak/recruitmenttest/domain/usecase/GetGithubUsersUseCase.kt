package com.spinarak.recruitmenttest.domain.usecase

import com.spinarak.recruitmenttest.common.usecase.FlowUseCase
import com.spinarak.recruitmenttest.domain.model.Users
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGithubUsersUseCase @Inject constructor(
        private val socialUsersRepository: SocialUsersRepository.Remote
) : FlowUseCase<Users?> {
    override fun execute() : Flow<Users?>{
        return socialUsersRepository.getGithubUsers()
    }
}