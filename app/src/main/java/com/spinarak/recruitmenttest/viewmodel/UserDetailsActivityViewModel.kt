package com.spinarak.recruitmenttest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.usecase.GetSocialUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserDetailsActivityViewModel @Inject constructor(
        private val socialUserLocalUseCase: GetSocialUserUseCase
) : ViewModel() {

    val user: LiveData<User>
        get() = _user
    private val _user: MutableLiveData<User> = MutableLiveData()

    fun userDetails(userId: String) {
        viewModelScope.launch {
            val userById : Flow<User> = socialUserLocalUseCase.execute(userId)
            userById.collect {
                _user.postValue(it)
            }
        }
    }

}