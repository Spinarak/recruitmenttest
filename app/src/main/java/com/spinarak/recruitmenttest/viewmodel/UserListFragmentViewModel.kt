package com.spinarak.recruitmenttest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.usecase.GetSocialUsersLocalUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserListFragmentViewModel @Inject constructor(
        private val getSocialUsersLocalUseCase: GetSocialUsersLocalUseCase
) : ViewModel() {

    val usersList: LiveData<List<User>>
        get() = _usersList

    private val _usersList: MutableLiveData<List<User>> = MutableLiveData()

    suspend fun getListOfUsers() {
        viewModelScope.launch {
            getSocialUsersLocalUseCase.execute().collect {
                _usersList.postValue(it)
            }
        }
    }
}