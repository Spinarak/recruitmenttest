package com.spinarak.recruitmenttest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.spinarak.recruitmenttest.domain.model.User
import com.spinarak.recruitmenttest.domain.model.Users
import com.spinarak.recruitmenttest.domain.repository.SocialUsersRepository
import com.spinarak.recruitmenttest.domain.usecase.GetDailymotionUsersUseCase
import com.spinarak.recruitmenttest.domain.usecase.GetGithubUsersUseCase
import com.spinarak.recruitmenttest.domain.usecase.SetSocialUsersLocalUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
        private val getDailymotionUsersUseCase: GetDailymotionUsersUseCase,
        private val getGithubUsersUseCase: GetGithubUsersUseCase,
        private val socialUsersUseCase: SetSocialUsersLocalUseCase
) : ViewModel() {
    fun saveUsers() {
        viewModelScope.launch {
            val savedUser : Flow<Users> = merge(
                getDailymotionUsersUseCase.execute(),
                getGithubUsersUseCase.execute()
            ).mapNotNull { it }

            savedUser.collect {
                socialUsersUseCase.execute(it.usersList)
            }

            merge(
                getDailymotionUsersUseCase.execute(),
                getGithubUsersUseCase.execute()
            )
                .mapNotNull { it }
                .collect {
                    socialUsersUseCase.execute(it.usersList)
                }
        }
    }

}