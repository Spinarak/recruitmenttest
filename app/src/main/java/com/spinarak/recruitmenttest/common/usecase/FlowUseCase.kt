package com.spinarak.recruitmenttest.common.usecase

import kotlinx.coroutines.flow.Flow

interface FlowUseCase<Type> {
    fun execute(): Flow<Type>
}
