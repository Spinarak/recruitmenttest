package com.spinarak.recruitmenttest.common.extension

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch

fun <T> Flow<T>.handleErrors(showErrorMessage: (e: Throwable) -> Unit): Flow<T> =
        catch { e -> showErrorMessage(e) }