package com.spinarak.recruitmenttest.common

object Consts {

    //BASE URLs
    val GITHUB_BASE_URL = "https://api.github.com/"
    val DAILYMOTION_BASE_URL = "https://api.dailymotion.com/"

    //API NAMES
    val GITHUB_API_NAME = "GITHUB"
    val DAILYMOTION_API_NAME = "DAILYMOTION"

    //API SERVICE
    val CONTENT_TYPE = "application/json"
}