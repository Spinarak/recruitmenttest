package com.spinarak.recruitmenttest.common.usecase

import kotlinx.coroutines.flow.Flow

interface FlowParameterUseCase<in Params, Type> {
    suspend fun execute(params: Params): Flow<Type>
}