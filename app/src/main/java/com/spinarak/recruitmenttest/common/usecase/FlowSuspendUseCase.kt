package com.spinarak.recruitmenttest.common.usecase

import kotlinx.coroutines.flow.Flow

interface FlowSuspendUseCase<Type> {
    suspend fun execute(): Flow<Type>
}